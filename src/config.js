const dotenv = require('dotenv');
const convict = require('convict');

dotenv.config();

const config = convict({
  env: {
    doc: 'Execution environment',
    format: ['production', 'development', 'test'],
    default: 'development',
    arg: 'nodeEnv',
    env: 'NODE_ENV',
  },
  db: {
    dbHost: {
      doc: 'mongo db host',
      format: String,
      default: 'localhost',
      arg: 'dbHost',
      env: 'DB_HOST',
    },
    dbPort: {
      doc: 'mongo db port',
      format: String,
      default: 'localhost',
      arg: 'dbPort',
      env: 'DB_PORT',
    },
    dbName: {
      doc: 'mongo db name',
      format: String,
      default: 'market',
      arg: 'dbDatabase',
      env: 'dbName',
    },
  },
});


module.exports = config;
