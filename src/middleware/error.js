const { HttpError } = require('http-errors');
const logger = require('../common/logger');

module.exports = async (err, req, res) => {
  if (err instanceof HttpError) {
    logger.error(err);

    const errStatus = err.statusCode || err.status;
    const errObj = {
      status: 'error',
      error: err.expose ? err.name : 'Error',
      message: err.expose ? err.message : '',
    };

    res.status(errStatus).json(errObj);
  } else {
    logger.error('Uncaught Exception', err);

    const errStatus = 500;
    const errObj = {
      status: 'error',
      message: 'Internal Server Error',
    };

    res.status(errStatus).json(errObj);
  }
};
