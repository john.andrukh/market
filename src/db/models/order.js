const { Schema, model } = require('mongoose');

const orderSchema = new Schema({
  totalAmount: {
    type: Number,
    default: 0,
    required: true,
  },
  sumWithDiscount: {
    type: Number,
    default: 0,
    required: true,
  },
  discounts: [{ type: Schema.Types.ObjectId }],
  currency: {
    type: String,
    default: 'USD',
  },
  orderDate: {
    type: Date,
    required: true,
    default: Date.now(),
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = model('Order', orderSchema);
