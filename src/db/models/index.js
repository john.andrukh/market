const Product = require('./product');
const Order = require('./order');
const OrderItem = require('./order-item');
const Discount = require('./discount');

module.exports = {
  Product,
  Order,
  OrderItem,
  Discount,
};
