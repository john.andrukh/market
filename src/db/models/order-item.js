const { Schema, model } = require('mongoose');

const orderItemSchema = new Schema({
  product: {
    type: String,
    required: true,
    ref: 'Product',
  },
  orderId: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'Order',
  },
  quantity: {
    type: Number,
    default: 1,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = model('OrderItem', orderItemSchema);
