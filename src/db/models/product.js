const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
    trim: true,
    set(name) {
      return name.toLowerCase();
    },
  },
  price: {
    type: Number,
    default: 0,
    required: true,
    unique: true,
    trim: true,
  },
  currency: {
    type: String,
    default: 'USD',
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = mongoose.model('Product', productSchema);
