const { Schema, model } = require('mongoose');

const discountSchema = new Schema({
  discountCreationEntity: {
    type: String,
    trim: true,
    default: null,
    set(name) {
      return name.toLowerCase();
    },
  },
  discountCreationQuantity: {
    type: String,
    trim: true,
    default: null,
  },
  discount: {
    type: Number,
    default: null,
    max: 99,
    required: true,
  },
  discountedItem: {
    type: String,
    trim: true,
    required: true,
  },
  dateFrom: {
    type: Date,
    default: null,
  },
  dateTo: {
    type: Date,
    default: null,
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = model('Discount', discountSchema);
