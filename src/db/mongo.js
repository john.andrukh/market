const mongoose = require('mongoose');

const config = require('../config');
const logger = require('../common/logger');

const { dbHost, dbName, dbPort } = config.get('db');

class Mongoose {
  constructor() {
    this.connection = mongoose.connect(`mongodb://${dbHost}:${dbPort}/${dbName}`, {
      useNewUrlParser: true,
      useCreateIndex: true,
    });

    const dbConnection = mongoose.connection;
    dbConnection.on('error', err => logger.error('Error connection to Mongo', err));
    dbConnection.once('open', () => logger.info('Successfully connected to Mongo'));
  }
}

module.exports = new Mongoose();
