const joi = require('joi');
const { BadRequest, UnprocessableEntity } = require('http-errors');

const validateInput = (params, schema) => {
  try {
    const { error } = joi.validate(params, schema);
    if (error) {
      throw new BadRequest(error.message);
    }
  } catch (error) {
    throw new UnprocessableEntity(error);
  }
};

module.exports = { validateInput };
