const orderService = require('./service');

const create = async (req, res, next) => {
  try {
    const { orderDate, items } = req.body;

    const newOrder = await orderService.create({ orderDate, items });

    return res.status(201).json(newOrder);
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  create,
};
