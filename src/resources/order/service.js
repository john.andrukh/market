/* eslint-disable max-len */
const { BadRequest } = require('http-errors');
const Moment = require('moment');
const MomentRange = require('moment-range');

const moment = MomentRange.extendMoment(Moment);

const { Order, OrderItem } = require('../../db/models');
const discountService = require('../discount/service');
const productService = require('../product/service');

const create = async ({ orderDate, items }) => {
  const productNames = items.map(item => item.product);
  const products = await productService.list({ name: productNames });

  items.forEach(item => {
    const existingProduct = products.find(pr => pr.name === item.product);

    if (!existingProduct) {
      throw new BadRequest(`Product with name ${item.product} does not exist`);
    }
  });

  const timeRangeDiscounts = await discountService.list({ discountCreationEntity: null });
  const creationEntityDiscounts = await discountService.list({ dateFrom: null });
  const discountsInfo = []; // info about discounts that were used in order


  const productsWithPrice = items.map(item => { // TODO: move to the separate service method
    const { price = 0 } = products.find(pr => pr.name === item.product);
    const totalPrice = item.quantity * price;
    let discountAmount = 0;

    if (timeRangeDiscounts.length) { // Process time range discount
      const discount = timeRangeDiscounts.find(disc => disc.discountedItem === item.product);

      if (discount) {
        const discountRange = moment.range(moment(discount.dateFrom), moment(discount.dateTo));

        if (moment(orderDate).within(discountRange)) {
          discountAmount += totalPrice * (discount.discount / 100);
          discountsInfo.push({
            id: discount.id,
            name: discount.discountedItem,
            discount: discount.discount,
            discountAmount,
          });
        }
      }
    }

    let discountCreationEntity = 0;
    if (creationEntityDiscounts.length) { // Process creationEntity discount
      const discount = creationEntityDiscounts.find(disc => disc.discountedItem === item.product);

      if (discount) {
        const itemCreationEntity = items.find(itemCre => itemCre.product === discount.discountCreationEntity && itemCre.quantity >= discount.discountCreationQuantity);

        if (itemCreationEntity) {
          const creationQuantity = itemCreationEntity.quantity;

          for (let i = item.quantity; i > 0; i -= 1) {
            if (creationQuantity >= Number(discount.discountCreationQuantity)) {
              discountCreationEntity += price * discount.discount / 100;
              creationQuantity.quantity = creationQuantity - Number(discount.discountCreationQuantity);
            }
          }

          discountsInfo.push({
            id: discount.id,
            name: discount.discountedItem,
            discount: discount.discount,
            discountAmount: discountCreationEntity,
          });
        }
      }
    }

    const totalDiscount = discountAmount + discountCreationEntity;

    return {
      price,
      totalPrice,
      totalDiscount,
      name: item.product,
      quantity: item.quantity,
    };
  });

  const { totalAmount, totalDiscount } = productsWithPrice.reduce((acc, item) => {
    acc.totalAmount += item.totalPrice;
    acc.totalDiscount += item.totalDiscount;
    return acc;
  }, { totalAmount: 0, totalDiscount: 0 });

  const discountIds = discountsInfo.map(discount => discount.id);

  const newOrder = await Order.create({// create order
    totalAmount,
    sumWithDiscount: totalAmount - totalDiscount,
    discounts: discountIds,
    orderDate,
  });

  await Promise.all(items.map(orderItem => OrderItem.create({// create order items
    orderId: newOrder.id,
    product: orderItem.product,
    quantity: orderItem.quantity,
  })));

  return {
    subtotal: totalAmount,
    total: totalAmount - totalDiscount,
    discounts: discountsInfo,
  };
};

module.exports = { create };
