const joi = require('joi');
const { addMinutes } = require('date-fns');

const { validateInput } = require('../../common/joi-validator');

const create = (req, res, next) => {
  const createSchema = joi.object().keys({
    orderDate: joi.date().min(addMinutes(new Date(), 10)).required(),
    items: joi.array().items(
      joi.object({
        product: joi.string().lowercase().required(),
        quantity: joi.number().min(1).max(10).required(),
      }).required(),
    ).unique('product').required(),
  });

  validateInput(req.body, createSchema);

  return next();
};

module.exports = {
  create,
};
