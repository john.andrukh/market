const joi = require('joi');

const { validateInput } = require('../../common/joi-validator');

const create = (req, res, next) => {
  const createSchema = joi.object().keys({
    name: joi.string().lowercase().required(),
    price: joi.number().required(),
  });

  validateInput(req.body, createSchema);

  return next();
};

module.exports = {
  create,
};
