const { Product } = require('../../db/models');

const create = async productInput => {
  const { name, price } = productInput;
  const newProduct = await Product.create({ name, price });

  return newProduct;
};

const list = async (filter = {}) => Product.find(filter);

module.exports = { create, list };
