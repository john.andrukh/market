const express = require('express');

const validator = require('./validator');
const controller = require('./controller');

const router = express.Router();

router.post('/api/products', validator.create, controller.create);
router.get('/api/products', controller.list);

module.exports = router;
