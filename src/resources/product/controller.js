const userService = require('./service');

const create = async (req, res, next) => {
  try {
    const { name, price } = req.body;

    const newProduct = await userService.create({ name, price });

    return res.status(201).send(newProduct);
  } catch (error) {
    next(error);
  }
};

const list = async (req, res, next) => {
  try {
    const products = await userService.list();

    return res.send(products);
  } catch (error) {
    next(error);
  }
};

module.exports = { create, list };
