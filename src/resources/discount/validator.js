const joi = require('joi');
const { addMinutes } = require('date-fns');

const { validateInput } = require('../../common/joi-validator');

const create = (req, res, next) => {
  const createSchema = joi.alternatives().try(
    joi.object().keys({
      discount: joi.number().required().max(99),
      discountedItem: joi.string().lowercase().required(),
      dateFrom: joi.date().min(addMinutes(new Date(), 10)).required(),
      dateTo: joi.date().required(),
    }),
    joi.object().keys({
      discountCreationEntity: joi.string().required(),
      discountCreationQuantity: joi.number().required(),
      discount: joi.number().required(),
      discountedItem: joi.string().required(),
      discountedItemQuantity: joi.number().required(),
    }),
  );

  validateInput(req.body, createSchema);

  return next();
};

module.exports = {
  create,
};
