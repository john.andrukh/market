const { Discount } = require('../../db/models');

const create = async discountParams => {
  let discount = {
    discount: discountParams.discount,
    discountedItem: discountParams.discountedItem,
  };

  if (discountParams.discountCreationEntity) {
    const {
      discountCreationEntity,
      discountCreationQuantity,
      discountedItemQuantity,
    } = discountParams;

    discount = {
      discountCreationEntity,
      discountCreationQuantity,
      discountedItemQuantity,
      ...discount,
    };
  } else {
    const { dateFrom, dateTo } = discountParams;
    discount = { dateFrom, dateTo, ...discount };
  }

  const newDiscount = await Discount.create(discount);

  return newDiscount;
};

const list = filter => Discount.find(filter);

module.exports = { create, list };
