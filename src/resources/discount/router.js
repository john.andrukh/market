const router = require('express').Router();

const validator = require('./validator');
const controller = require('./controller');

router.post('/api/discounts', validator.create, controller.create);

module.exports = router;
