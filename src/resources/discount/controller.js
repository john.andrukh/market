const discountService = require('./service');

const create = async (req, res, next) => {
  try {
    const newDiscount = await discountService.create(req.body);

    return res.status(201).json(newDiscount);
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  create,
};
