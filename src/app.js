const express = require('express');
const cors = require('cors');

const app = express();

const Product = require('./resources/product/router');
const Discount = require('./resources/discount/router');
const Order = require('./resources/order/router');

const logger = require('./common/logger');
const errorMiddlware = require('./middleware/error');

require('./db/mongo');


app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());

app.use(Product);
app.use(Discount);
app.use(Order);

app.use(errorMiddlware);

app.listen(8000, () => logger.info('App listening on port 8000!'));
