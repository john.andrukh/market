# Market project

Simple REST project.

# Technologies which have been used:
* NodeJS
* Express
* MongoDB

# Requirements:
* Installed Docker - https://docs.docker.com/install/
* Installed docker-compose - https://docs.docker.com/compose/install/
* Installed NodeJS and npm - https://nodejs.org/en/download/
* Installed Robo3T for MongoDb - https://robomongo.org/

# Setup project:
* git clone https://gitlab.com/john.andrukh/market - to download the project;
* `npm install` - to install all project dependencies

# Configuration
* Create `.env` file - and pass variables here
* Import `market.postman_collection.json` file with tests requests to Postman

| Environment variables           | Default value | Description           |
| --------------------------------| ------------- | --------------------- |
| `NODE_ENV`                      | development   |                       |
| `DB_HOST`                       | localhost     | mongo host            |
| `DB_PORT`                       | 27017         | mongo port            |
| `DB_NAME`                       | market        | mongo db name         |
| `LOG_LEVEL`                     | debug         | logger log level      |

# Run locally
* `docker-compose up` - to start mongoDb server
* `npm start` - to start Node server
